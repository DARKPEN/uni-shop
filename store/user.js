export default {
  // 为当前模块开启命名空间
  namespaced: true,
  // 模块的 state 数据
  state: () => ({
    address: JSON.parse(uni.getStorageSync('address') || '{}'),
    token: uni.getStorageSync('token') || '',
    userinfo: JSON.parse(uni.getStorageSync('userinfo') || '{}'),
    redirectInfo: null
  }),
  // 模块的 mutations 方法
  mutations: {
    updateAddress(state, address) {
      state.address = address
      this.commit('m_user/SaveAddressToStorage')
    },
    SaveAddressToStorage(state) {
      uni.setStorageSync('address', JSON.stringify(state.address))
    },
    updataUserInfo(state, userInfo) {
      state.userinfo = userInfo
      this.commit('m_user/saveUserInfoToStorage')
    },
    saveUserInfoToStorage(state) {
      uni.setStorageSync('userinfo', JSON.stringify(state.userinfo))
    },
    // 更新 token 字符串
    updateToken(state, token) {
      state.token = token
      // 通过 this.commit() 方法，调用 m_user 模块下的 saveTokenToStorage 方法，将 token 字符串持久化存储到本地
      this.commit('m_user/saveTokenToStorage')
    },

    // 将 token 字符串持久化存储到本地
    saveTokenToStorage(state) {
      uni.setStorageSync('token', state.token)
    },
      updateRedirectInfo(state, info) {
        state.redirectInfo = info
      }
  },

  // 模块的 getters 属性
  getters: {
    addstr(state) {
      if (!state.address.provinceName) return
      return state.address.provinceName + state.address.cityName + state.address.countyName + state.address.detailInfo
    }
  }
}
