export default {
  // 为当前模块开启命名空间
  namespaced: true,
  // 模块的 state 数据
  state: () => ({
    // { goods_id, goods_name, goods_price, goods_count, goods_small_logo, goods_state }
    cart: JSON.parse(uni.getStorageSync('cart') || '[]')
  }),
  // 模块的 mutations 方法
  mutations: {
    addToCart(state, goods) {
      const findResult = state.cart.find(item => item.goods_id === goods.goods_id)
      if (!findResult) {
        state.cart.push(goods)
      } else {
        findResult.goods_count++
      }
      uni.setStorageSync('cart', JSON.stringify(state.cart))
    },
    updateGoodsState(state, goods) {
      const findResult = state.cart.find(x => x.goods_id === goods.goods_id)

      // 有对应的商品信息对象
      if (findResult) {
        // 更新对应商品的勾选状态
        findResult.goods_state = goods.goods_state
        // 持久化存储到本地
      }
      uni.setStorageSync('cart', JSON.stringify(state.cart))
    },
    updateGoodsCount(state, goods) {
      // 根据 goods_id 查询购物车中对应商品的信息对象
      const findResult = state.cart.find(x => x.goods_id === goods.goods_id)

      if (findResult) {
        // 更新对应商品的数量
        findResult.goods_count = goods.goods_count
      }
      uni.setStorageSync('cart', JSON.stringify(state.cart))
    },
    removeGoodsById(state, goods) {
      state.cart = state.cart.filter(item => item.goods_id !== goods.goods_id)
      uni.setStorageSync('cart', JSON.stringify(state.cart))
    },
    updateAllGoodsState(state, goods) {
      state.cart.forEach(item => item.goods_state = goods)
      uni.setStorageSync('cart', JSON.stringify(state.cart))
    }
  },

  // 模块的 getters 属性
  getters: {
    // 统计购物车中商品的总数量
    total(state) {
      let c = 0
      // 循环统计商品的数量，累加到变量 c 中
      state.cart.forEach(goods => c += goods.goods_count)
      return c
    },
    isFullCheck(state) {
      return state.cart.every(item => item.goods_state)
    },
    checkedCount(state) {
      return state.cart.filter(x => x.goods_state).reduce((total, item) =>
        total += item.goods_count, 0)
    },
    checkedGoodsAmount(state) {
      return state.cart.filter(x => x.goods_state)
        .reduce((total, item) =>
          total += item.goods_count * item.goods_price, 0).toFixed(2)
    }
  }
}
